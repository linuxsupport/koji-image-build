# Cloud images

Refer to [CHANGELOG](./CHANGELOG.md) to see the latest changes

## Image building

note: docker images moved to separate projects (linuxsupport/{c8-base,cc7-base,slc6-base,slc5-base})

### How to build:

See ```buildimage.sh``` script.

## Images:

All images are made available in 'IT Linux Support - CI VMs' Openstack.cern.ch Project. (See ```upload2openstack.sh``` script.)

### Publishing Images

There are Gitlab CI jobs that make new images public and mark the old ones as 'community'.
If for some reason you would like to exclude a public image from this process, you can edit
it's metadata to change the value of the property `gitops`.

[Tests](https://gitlab.cern.ch/linuxsupport/testing/image-ci) are in place to check everything is working as expected. If you want to skip them, run a pipeline with variable `CI_SKIP_TESTS="true"`. This can be done through CI/CD -> Pipelines -> Run pipeline -> Select branch, add the variable, press run. This will override globally defined variables in the CI.

A scheduled pipeline rebuilds the image once per month.

#### Red Hat Enterprise Linux images

As of August 2021, stages exist to automatically download and convert upstream RHEL images from the RHSM, for use in the CERN cloud.
Previously this was a manual task which required configuration of libvirt and other software on a host VM.
Whilst no longer required, if needed the prerequisites are still documented [here](./RHEL_CONVERT_ENVIRONMENT.md).

RHEL images are tested via image-ci and marked as community. An email is sent informing RHEL users of the new RHEL image uuid

#### Error: Bootstrap repo needs updating before continuing

You may will need to update the bootstrap repo in <http://linuxsoft.cern.ch/internal/bootstrap/rhel{7,8}/x86_64/> with the package version that the error mentions.

Normally due to dependencies it will correspond to 3 packages you will need to add, all of them having the same version.

As `root`, on `lxsoftadm01`:

```bash
cp /mnt/data1/dist/cdn.redhat.com/content/dist/rhel/server/7/7Server/x86_64/os/Packages/k/krb5-libs-1.15.1-50.el7.x86_64.rpm /mnt/data1/dist/internal/bootstrap/rhel7/x86_64/Packages/
cp /mnt/data1/dist/cdn.redhat.com/content/dist/rhel/server/7/7Server/x86_64/os/Packages/k/krb5-workstation-1.15.1-50.el7.x86_64.rpm /mnt/data1/dist/internal/bootstrap/rhel7/x86_64/Packages/
cp /mnt/data1/dist/cdn.redhat.com/content/dist/rhel/server/7/7Server/x86_64/os/Packages/l/libkadm5-1.15.1-50.el7.x86_64.rpm /mnt/data1/dist/internal/bootstrap/rhel7/x86_64/Packages/
# Delete old package versions before retrying !!!
rm /mnt/data1/dist/internal/bootstrap/rhel7/x86_64/Packages/libkadm5-1.15.1-46.el7.x86_64.rpm
rm /mnt/data1/dist/internal/bootstrap/rhel7/x86_64/Packages/krb5-libs-1.15.1-46.el7.x86_64.rpm
rm /mnt/data1/dist/internal/bootstrap/rhel7/x86_64/Packages/krb5-workstation-1.15.1-46.el7.x86_64.rpm
# Update repodata or your yum update will not get the new packages
cd /mnt/data1/dist/internal/bootstrap/rhel7/x86_64/
createrepo -d .
```

After this, simply retry your rhel build gitlab ci job

#### CERN CentOS 7.3 image for CMS

Share with 'CMS Data Acquisition':

CC7 - x86_64 [2017-01-24] 77b4efd5-dfb4-4330-81a1-d930184b2533
CC7 TEST - x86_64 [2017-01-13] 43c71fe1-71b8-4225-8a37-58c9279914f5

```openstack image add project XXX 63176c63-4cb9-4bcc-bdaf-158437b881c5```
ask to accept with:
```openstack image set --accept XXX```

### NOTES:

slc6 -> ksversion RHEL7 <- otherwise kickstart %packages --nocore not acccepted.

slc5 -> ksversion RHEL5 <- otherwise kickstart sections not ending in %end fail.

