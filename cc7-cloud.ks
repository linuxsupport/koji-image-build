#version=RHEL7
#perform install
install
# System authorization information
auth --enableshadow --passalgo=sha512
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration AFS and ARCD
firewall --enabled --service=ssh --port=7001:udp --port=4241:tcp
# No firstboot
firstboot --disable
# Network information
network  --bootproto=dhcp
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump,rhsmcertd,wpa_supplicant" --enabled="network,sshd,rsyslog"
# License agreement
eula --agreed
# System timezone
timezone --utc Europe/Zurich --ntpservers ip-time-0.cern.ch,ip-time-1.cern.ch,ip-time-2.cern.ch
# this should not be neeed
ignoredisk --only-use=vda
# Clear the Master Boot Record
zerombr

# Partitioning and bootloader configuration
# Note: biosboot and efi partitions are pre-created in %pre.
# Based on https://github.com/openlogic/AzureBuildCentOS/blob/8f46a34e6fceed41030ef4611ca85a272f0abbe4/ks/azure/centos77.ks

bootloader --location=mbr --timeout=1 --append="console=ttyS0,115200 console=tty0" --boot-drive=vda
# This is replaced by the pre-creation of partitions by sgdisk
#part biosboot --onpart=vda14 --size=4
part /boot/efi --onpart=vda15 --fstype=vfat --label=EFI
# We specify a minimum size for root. Do not use --grow as some people might want to avoid it with https://clouddocs.web.cern.ch/using_openstack/contextualisation.html#dont-grow-the-underlying-partition
# growpart will take care of resizing it anyway by default if not disabled
part / --fstype="xfs" --size=3000 --mkfsoptions="-n ftype=1" --label=ROOT

%pre --log=/var/log/anaconda/pre-install.log --erroronfail
ARCH=`uname -m`
cat >> /etc/rsyslog.conf  <<DELIM
\$template AnacondaTemplate, "<%PRI%>%TIMESTAMP:::date-rfc3339% image:cc7-cloud-$ARCH %syslogtag:1:32%%msg:::sp-if-no-1st-sp%%msg%"
\$ActionForwardDefaultTemplate AnacondaTemplate

module(load="imfile" mode="inotify")
input(type="imfile"
  File="/tmp/anaconda.log"
  Tag="anaconda")
input(type="imfile"
  File="/tmp/dnf.librepo.log"
  Tag="dnf-librepo")
input(type="imfile"
  File="/tmp/packaging.log"
  Tag="packaging")
input(type="imfile"
  File="/var/log/anaconda/pre-install.log"
  Tag="ks-pre")
input(type="imfile"
  File="/mnt/sysroot/root/anaconda-post.log"
  Tag="ks-post")

*.* action(type="omfwd" queue.type="direct" target="linuxsoftadm.cern.ch" port="5014" protocol="tcp")
DELIM
/usr/bin/systemctl restart rsyslog

# Pre-create the biosboot and EFI partitions
#  - Ensure that efi and biosboot are created at the start of the disk to
#    allow resizing of the OS disk.
#  - Label biosboot and efi as vda14/vda15 for better compat - some tools
#    may assume that vda1/vda2 are '/boot' and '/' respectively.
sgdisk --clear /dev/vda
sgdisk --new=14:2048:10239 /dev/vda
# Size must be 50MB at least
sgdisk --new=15:10240:550M /dev/vda
# Add hex codes to label partitions appropriately as KS part command does
# EF02 hex code: BIOS boot partition
sgdisk --typecode=14:EF02 /dev/vda
# EF00 hex code: EFI System
sgdisk --typecode=15:EF00 /dev/vda
%end


# overlay for XFS/docker: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/7.2_Release_Notes/technology-preview-file_systems.html
# Reboot after installation
reboot

repo --name="CentOS-7 - Base" --baseurl http://linuxsoft.cern.ch/cern/centos/7.9/os/$basearch/
# The following repo lines are commented as our koji tag builds already define these as external repos
#repo order is *important* for koji image build target !
#repo --name="CentOS-7 - CERN" --baseurl http://linuxsoft.cern.ch/cern/centos/7/cern/$basearch/
#repo --name="CentOS-7 - Updates" --baseurl http://linuxsoft.cern.ch/cern/centos/7/updates/$basearch/
#repo --name="CentOS-7 - Extras" --baseurl http://linuxsoft.cern.ch/cern/centos/7/extras/$basearch/

%packages
@cern-base
chrony
dracut-config-generic
dracut-norescue
firewalld
grub2
# For cases where the bootloader needs to be relocated, like Software RAID setups, we need grub2-efi-x64-modules and efibootmgr
grub2-efi-x64-modules
efibootmgr
parted
# Needed for manual partitioning for dual booting
gdisk
iptables-services
kernel
krb5-workstation
redhat-lsb-core
nfs-utils
ntp
rsync
tar
vim-enhanced
yum-utils
yum-plugin-ovl
mdadm
-aic94xx-firmware
-alsa-firmware
-alsa-lib
-alsa-tools-firmware
-biosdevname
-lcm-firstboot
-iprutils
-ivtv-firmware
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6000g2b-firmware
-iwl6050-firmware
-iwl7260-firmware
-iwl7265-firmware
-libertas-sd8686-firmware
-libertas-sd8787-firmware
-libertas-usb8388-firmware
-linux-firmware
-mcelog
-plymouth
-xorg-x11-font-utils
cloud-init
# Users can disable growpart with https://clouddocs.web.cern.ch/using_openstack/contextualisation.html#dont-grow-the-underlying-partition
cloud-utils-growpart
cern-private-cloud-addons
mesa-libGL
locmap
nscd
sendmail-cf
%end

# not taken into account by image factory ...
%addon cern_customizations  --run-locmap --afs-client --auto-update
%end

%post --log=/root/anaconda-post.log

# lock root account
passwd -d root
passwd -l root

# Generate new machine-id on first boot
truncate --size 0 /etc/machine-id
# Ensure systemd/random-seed is well, random
truncate --size 0 /var/lib/systemd/random-seed

yum -y update
rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# initscripts don't like this file to be missing.
cat > /etc/sysconfig/network << EOF
NETWORKING=yes
NOZEROCONF=yes
EOF

# simple eth0 config, again not hard-coded to the build hardware
rm -f /etc/sysconfig/network-scripts/ifcfg-*
cat > /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
NAME="eth0"
DEVICE="eth0"
ONBOOT="yes"
NETBOOT="yes"
IPV6INIT="yes"
BOOTPROTO="dhcp"
TYPE="Ethernet"
DEFROUTE="yes"
IPV4_FAILURE_FATAL="no"
IPV6_AUTOCONF="yes"
IPV6_DEFROUTE="yes"
IPV6_FAILURE_FATAL="no"
DHCPV6_DUID="llt"
USERCTL="yes"
PEERDNS="yes"
PERSISTENT_DHCLIENT="1"
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# fix rtc to use utc not local time (INC0974179)
# done via --utc timzezone switch
#timedatectl set-local-rtc 0
#cat >> /etc/adjtime << EOF
#0.0 0 0.0
#0
#UTC
#EOF

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl disable tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: .*|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
    # and fix it again !
    sed -i -e 's/\[CLOUDINIT\]/cloud-init/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/syslogtag/programname/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/cloud-init.log/cloud-init-output.log/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/*

# enable locmap default modules
locmap --enable afs
locmap --enable kerberos
locmap --enable lpadmin
locmap --enable nscd
locmap --enable ntp
locmap --enable sendmail
locmap --enable ssh
locmap --enable sudo


# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/yum/*

# Ironic fix for RAID1 at boot time
/usr/bin/dracut -v --add 'mdraid' --add-drivers 'raid1 raid10 raid5 raid0 linear' --regenerate-all --force

# Add rd.auto and net.ifnames=0 for Ironic (grubby doesn't seem to work well in CC7)
sed -i '/GRUB_CMDLINE_LINUX=/ s/"$/ rd.auto net.ifnames=0"/' /etc/default/grub

# We are installing bootloader with the bootloader command, but this only adds uefi compatible (as we are building in a UEFI VM), so we force both: https://unix.stackexchange.com/questions/273329/can-i-install-grub2-on-a-flash-drive-to-boot-both-bios-and-uefi
# Enable BIOS bootloader
grub2-mkconfig --output /etc/grub2-efi.cfg
grub2-install --target=i386-pc --directory=/usr/lib/grub/i386-pc/ /dev/vda
# Notes regarding dual booting and grub configuration!!!
# On a BIOS machine grub configuration will be /boot/grub2/grub.cfg, on a UEFI one, /boot/efi/EFI/centos/grub.cfg
# Grub Environmental Block (grubenv) on a BIOS machine is /boot/grub2/grubenv and /boot/efi/EFI/centos/grubenv on UEFI, BUT since
# we are building on a UEFI VM, /boot/grub2/grubenv will always be a symlink to /boot/efi/EFI/centos/grubenv, i.e. both booting modes
# will be using the same file which is on the EFI partition. This means we will need this EFI partition (#15) even on BIOS machines!
grub2-mkconfig --output=/boot/grub2/grub.cfg


# Fix grub.cfg to remove EFI entries and adapt them to BIOS

# We need to adapt grub configuration in the case of BIOS config as it is preloaded with the UEFI partition ID instead of the BIOS partition ID
EFI_ID=`blkid -s UUID -o value /dev/vda15`
BOOT_ID=`blkid -s UUID -o value /dev/vda1`
sed -i 's/gpt15/gpt1/' /boot/grub2/grub.cfg
sed -i "s/${EFI_ID}/${BOOT_ID}/" /boot/grub2/grub.cfg
# Force Grub Environmental Block to come from our common grubenv location
sed -i 's|$prefix/grubenv|(hd0,gpt15)/efi/centos/grubenv|' /boot/grub2/grub.cfg
sed -i 's|load_env|load_env -f (hd0,gpt15)/efi/centos/grubenv|' /boot/grub2/grub.cfg
sed -i '/^### BEGIN \/etc\/grub.d\/30_uefi/,/^### END \/etc\/grub.d\/30_uefi/{/^### BEGIN \/etc\/grub.d\/30_uefi/!{/^### END \/etc\/grub.d\/30_uefi/!d}}' /boot/grub2/grub.cfg
# Required for CentOS 7.x due to no blscfg: https://bugzilla.redhat.com/show_bug.cgi?id=1570991#c6
sed -i -e 's|linuxefi|linux|' -e 's|initrdefi|initrd|' /boot/grub2/grub.cfg


# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/BUILDTIME
%end

%post --nochroot
# Make rsyslog send everything before the reboot
pkill -HUP rsyslogd
sleep 30s
rm -f /mnt/sysroot/root/anaconda-{ks,pre,post}.log
%end
